{
  "TestId": "20221212-213029",
  "LambdasArray": [],
  "LambdaRange": [],
  "DiscoveryPort": 19000,
  "SchedulerPort": 18080,
  "LearnerPort": 19020,
  "BenchmarkTime": 1800,
  "Hosts": [
    "192.168.50.100",
    "192.168.50.101",
    "192.168.50.102",
    "192.168.50.103",
    "192.168.50.104",
    "192.168.50.105",
    "192.168.50.107",
    "192.168.50.110",
    "192.168.50.111",
    "192.168.50.112",
    "192.168.50.113"
  ],
  "HostsToNodeId": null,
  "HostsFileDir": "./hosts.txt",
  "FunctionName": "fn-pigo",
  "DirLogs": "./log",
  "DirPayloads": "./blobs",
  "DirTrafficModel": "./traffic",
  "Payloads": [
    "familyr_320p.jpg",
    "familyr_180p.jpg"
  ],
  "PayloadMixPercentages": [
    0.5,
    0.5
  ],
  "Learning": true,
  "DoubleLearning": true,
  "LearningRewardDeadlines": [
    0.18825,
    0.07495
  ],
  "LearningSetReward": true,
  "LearningBatchSize": 10,
  "TrafficModelFilenamePrefix": "traffic_node_",
  "TrafficModelType": "dynamic",
  "TrafficGenerationDistribution": 1
}